using System.Collections.Generic;
using System.Linq;
using Core.Controllers.HeroesPanel;
using Core.Controllers.HeroesPanel.HeroCards;
using Core.Controllers.MissionEnding;
using Core.Controllers.Missions;
using Core.Controllers.SelectedMission;
using Core.Controllers.SelectedMission.Window;
using Core.Data.Heroes;
using Core.Data.Missions;
using Core.Definitions;
using UnityEngine;

namespace Core
{
    public class EntryPoint : MonoBehaviour
    {
        [SerializeField] private List<MissionMarker> m_MissionMarkers;
        [SerializeField] private HeroesPanelView m_HeroesPanelView;
        [SerializeField] private SelectedMissionWindow m_LeftSelectedMissionWindow;
        [SerializeField] private SelectedMissionWindow m_RightSelectedMissionWindow;
        [SerializeField] private MissionEndingWindow m_MissionEndingWindow;
        [SerializeField] private HeroCardView m_HeroCardViewPrefab;
        [SerializeField] private MissionEndingHeroCardView m_MissionEndingHeroCardViewPrefab;

        private MissionsDataController m_MissionsDataController;
        private HeroesDataController m_HeroesDataController;
        private HeroesPanelController m_HeroesPanelController;
        private MissionEndingController m_MissionEndingController;
        private MissionEndingWindowController m_MissionEndingWindowController;
        private SelectedMissionWindowsController m_SelectedMissionWindowsController;
        private SelectedMissionController m_SelectedMissionController;
        private List<MissionController> m_MissionControllers;

        private DefsFacade DefsFacade => DefsFacade.I;

        private void Start()
        {
            Init();
        }

        private void Init()
        {
            ResolveDependencies();
            InitializeControllers();
        }

        private void ResolveDependencies()
        {
            ResolveData();
            ResolveHeroesPanel();
            ResolveMissionEnding();
            ResolveSelectedMission();
            ResolveMissions();
        }

        private void InitializeControllers()
        {
            m_HeroesDataController.Initialize();
            m_MissionsDataController.Initialize();

            m_HeroesPanelController.Initialize();

            m_MissionEndingWindowController.Initialize();
            
            m_SelectedMissionWindowsController.Initialize();
            m_SelectedMissionController.Initialize();

            foreach (var missionController in m_MissionControllers)
            {
                missionController.Initialize();
            }
        }

        private void ResolveData()
        {
            m_HeroesDataController = new HeroesDataController(DefsFacade.Heroes, DefsFacade.PlayerDefinition);
            m_MissionsDataController = new MissionsDataController(DefsFacade.Missions);
        }

        private void ResolveHeroesPanel()
        {
            m_HeroesPanelController = new HeroesPanelController(
                m_HeroesPanelView,
                m_HeroesDataController,
                DefsFacade.Heroes,
                m_HeroCardViewPrefab);
        }

        private void ResolveMissionEnding()
        {
            m_MissionEndingWindowController = new MissionEndingWindowController(
                m_MissionEndingWindow,
                m_MissionEndingHeroCardViewPrefab);

            m_MissionEndingController = new MissionEndingController(
                m_MissionEndingWindowController,
                DefsFacade.Missions,
                DefsFacade.Heroes);
        }

        private void ResolveSelectedMission()
        {
            m_SelectedMissionWindowsController = new SelectedMissionWindowsController(
                m_LeftSelectedMissionWindow,
                m_RightSelectedMissionWindow);
            
            m_SelectedMissionController = new SelectedMissionController(
                m_HeroesPanelController,
                m_SelectedMissionWindowsController,
                DefsFacade.Missions,
                m_MissionsDataController,
                m_HeroesDataController,
                m_MissionEndingController);
        }

        private void ResolveMissions()
        {
            m_MissionControllers = new List<MissionController>();
            foreach (var missionMarker in m_MissionMarkers)
            {
                var missionController = new MissionController(
                    missionMarker.Id,
                    missionMarker.View,
                    m_MissionsDataController,
                    m_SelectedMissionController,
                    DefsFacade.Missions);
                m_MissionControllers.Add(missionController);
            }
        }

#if UNITY_EDITOR
        [ContextMenu("Update Mission markers")]
        private void UpdateMissionMarkers()
        {
            var markers = FindObjectsOfType<MissionMarker>().ToList();
            m_MissionMarkers = markers;
        }
#endif
    }
}