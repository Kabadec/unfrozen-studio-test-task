﻿using System.Collections.Generic;
using Core.Definitions.Missions;
using Core.Definitions.Missions.AccessConditions;
using UnityEngine;

namespace Core.Data.Missions
{
    public class MissionsDataController : IInitializable
    {
        private readonly MissionsRepository m_MissionsRepository;

        private Dictionary<string, MissionData> m_MissionDatas;
        
        public MissionDataChangeEventProducer DataChangeEventProducer { get; private set; }

        public MissionsDataController(MissionsRepository missionsRepository)
        {
            m_MissionsRepository = missionsRepository;
        }

        public void Initialize()
        {
            m_MissionDatas = new Dictionary<string, MissionData>();
            DataChangeEventProducer = new MissionDataChangeEventProducer();
            
            foreach (var missionDefinition in m_MissionsRepository.Missions)
            {
                var missionData = new MissionData()
                {
                    MissionId = missionDefinition.Id,
                    IsCompleted = false,
                };
                
                m_MissionDatas.Add(missionDefinition.Id, missionData);
            }
            
            foreach (var missionDefinition in m_MissionsRepository.Missions)
            {
                m_MissionDatas[missionDefinition.Id].IsLocked = GetIsLocked(missionDefinition.AccessCondition);
            }
        }

        public bool GetIsLocked(IAccessConditionDefinition accessCondition)
        {
            if (accessCondition.AccessCondition.Count <= 0)
            {
                return false;
            }
            
            foreach (var missionSpecificationId in accessCondition.AccessCondition)
            {
                if (!GetMissionData(missionSpecificationId.MissionId, out var data))
                {
                    Debug.LogError($"{nameof(MissionsDataController)} >>> Data not found. Id: {missionSpecificationId.MissionId}");
                    continue;
                }

                if (data.IsCompleted)
                {
                    if (missionSpecificationId.SpecificationId == string.Empty || data.SpecificationId == missionSpecificationId.SpecificationId)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public void CompleteMission(string missionId, string specificationId)
        {
            if (!GetMissionData(missionId, out var missionData))
            {
                return;
            }

            missionData.IsCompleted = true;
            missionData.SpecificationId = specificationId;
            DataChangeEventProducer.NotifyAll(
                new MissionDataChangeEventArgs
                    (missionId, 
                    missionData.IsCompleted, 
                    missionData.SpecificationId)); 
        }

        public bool GetIsCompleted(string missionId)
        {
            if (!GetMissionData(missionId, out var mission))
            {
                return false;
            }

            return mission.IsCompleted;
        }

        private bool GetMissionData(string missionId, out MissionData missionData)
        {
            if (!m_MissionDatas.TryGetValue(missionId, out missionData))
            {
                Debug.LogError($"{nameof(MissionsDataController)} >>> Mission data not found. Id: {missionId}");
                return false;
            }

            return true;
        }
    }
}