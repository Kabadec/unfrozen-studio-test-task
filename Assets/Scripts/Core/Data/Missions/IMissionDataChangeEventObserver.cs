﻿namespace Core.Data.Missions
{
    public interface IMissionDataChangeEventObserver
    {
        void Notify(MissionDataChangeEventArgs eventArgs);
    }
}