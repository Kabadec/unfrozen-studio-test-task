﻿namespace Core.Data.Missions
{
    public class MissionData
    {
        public string MissionId;
        public bool IsLocked;
        public bool IsCompleted;
        public string SpecificationId;
    }
}