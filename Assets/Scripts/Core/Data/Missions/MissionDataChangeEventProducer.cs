﻿using System.Collections.Generic;

namespace Core.Data.Missions
{
    public class MissionDataChangeEventProducer
    {
        private readonly List<IMissionDataChangeEventObserver> m_Observers = new List<IMissionDataChangeEventObserver>();

        public void Attach(IMissionDataChangeEventObserver observer)
        {
            m_Observers.Add(observer);
        }
        
        public void Detach(IMissionDataChangeEventObserver observer)
        {
            m_Observers.Remove(observer);
        }

        public void NotifyAll(MissionDataChangeEventArgs eventArgs)
        {
            foreach (var observer in m_Observers)
            {
                observer?.Notify(eventArgs);
            }
        }
    }
}