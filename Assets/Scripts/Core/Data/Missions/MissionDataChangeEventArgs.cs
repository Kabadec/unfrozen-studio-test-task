﻿namespace Core.Data.Missions
{
    public class MissionDataChangeEventArgs
    {
        public string MissionId { get; }
        public bool IsCompleted { get; }
        public string SpecificationId { get; }

        public MissionDataChangeEventArgs(string missionId, bool isCompleted, string specificationId)
        {
            MissionId = missionId;
            IsCompleted = isCompleted;
            SpecificationId = specificationId;
        }
    }
}