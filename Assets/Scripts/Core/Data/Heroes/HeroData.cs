﻿namespace Core.Data.Heroes
{
    public class HeroData
    {
        public string Id;
        public bool Locked;
        public int Points;
    }
}