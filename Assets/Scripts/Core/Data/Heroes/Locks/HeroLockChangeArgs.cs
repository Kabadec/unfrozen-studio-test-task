﻿namespace Core.Data.Heroes.Locks
{
    public class HeroLockChangeArgs
    {
        public string HeroId { get; private set; }
        public bool Locked { get; private set; }

        public HeroLockChangeArgs(string heroId, bool locked)
        {
            HeroId = heroId;
            Locked = locked;
        }
    }
}