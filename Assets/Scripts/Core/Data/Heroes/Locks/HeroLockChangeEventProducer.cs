﻿using System.Collections.Generic;

namespace Core.Data.Heroes.Locks
{
    public class HeroLockChangeEventProducer
    {
        private readonly List<IHeroLockChangeEventObserver> m_Observers = new List<IHeroLockChangeEventObserver>();

        public void Attach(IHeroLockChangeEventObserver observer)
        {
            m_Observers.Add(observer);
        }
        
        public void Detach(IHeroLockChangeEventObserver observer)
        {
            m_Observers.Remove(observer);
        }

        public void NotifyAll(HeroLockChangeArgs args)
        {
            foreach (var observer in m_Observers)
            {
                observer?.Notify(args);
            }
        }
    }
}