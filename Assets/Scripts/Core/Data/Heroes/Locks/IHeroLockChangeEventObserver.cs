﻿namespace Core.Data.Heroes.Locks
{
    public interface IHeroLockChangeEventObserver
    {
        void Notify(HeroLockChangeArgs args);
    }
}