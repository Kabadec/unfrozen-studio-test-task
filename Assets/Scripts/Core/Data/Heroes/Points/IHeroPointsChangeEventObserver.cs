﻿namespace Core.Data.Heroes.Points
{
    public interface IHeroPointsChangeEventObserver
    {
        void Notify(HeroPointsChangeArgs args);
    }
}