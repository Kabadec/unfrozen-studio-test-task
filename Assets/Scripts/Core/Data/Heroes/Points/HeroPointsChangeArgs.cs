﻿namespace Core.Data.Heroes.Points
{
    public class HeroPointsChangeArgs
    {
        public string HeroId { get; private set; }
        public bool IsPointsIncreased { get; private set; }
        public int Points { get; private set; }

        public HeroPointsChangeArgs(string heroId, bool isPointsIncreased, int points)
        {
            HeroId = heroId;
            IsPointsIncreased = isPointsIncreased;
            Points = points;
        }
    }
}