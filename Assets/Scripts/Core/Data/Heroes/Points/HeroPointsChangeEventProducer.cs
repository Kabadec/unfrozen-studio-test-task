﻿using System.Collections.Generic;

namespace Core.Data.Heroes.Points
{
    public class HeroPointsChangeEventProducer
    {
        private readonly List<IHeroPointsChangeEventObserver> m_Observers = new List<IHeroPointsChangeEventObserver>();

        public void Attach(IHeroPointsChangeEventObserver observer)
        {
            m_Observers.Add(observer);
        }
        
        public void Detach(IHeroPointsChangeEventObserver observer)
        {
            m_Observers.Remove(observer);
        }

        public void NotifyAll(HeroPointsChangeArgs args)
        {
            foreach (var observer in m_Observers)
            {
                observer?.Notify(args);
            }
        }
    }
}