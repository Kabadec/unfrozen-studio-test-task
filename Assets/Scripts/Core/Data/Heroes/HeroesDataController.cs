﻿using System.Collections.Generic;
using Core.Data.Heroes.Locks;
using Core.Data.Heroes.Points;
using Core.Definitions.Heroes;
using Core.Definitions.Player;
using UnityEngine;

namespace Core.Data.Heroes
{
    public class HeroesDataController : IInitializable
    {
        private const int m_DefaultHeroPoints = 0;
        
        private readonly HeroesRepository m_HeroesRepository;
        private readonly PlayerDefinition m_PlayerDefinition;

        private Dictionary<string, HeroData> m_HeroDatas;

        public HeroPointsChangeEventProducer PointsChangeEventProducer { get; private set; }
        public HeroLockChangeEventProducer LockChangeEventProducer { get; private set; }

        public HeroesDataController(HeroesRepository heroesRepository,
            PlayerDefinition playerDefinition)
        {
            m_HeroesRepository = heroesRepository;
            m_PlayerDefinition = playerDefinition;
        }

        public void Initialize()
        {
            m_HeroDatas = new Dictionary<string, HeroData>();
            PointsChangeEventProducer = new HeroPointsChangeEventProducer();
            LockChangeEventProducer = new HeroLockChangeEventProducer();
            
            foreach (var heroDefinition in m_HeroesRepository.Heroes)
            {
                var heroData = new HeroData
                {
                    Id = heroDefinition.Id,
                    Locked = true,
                    Points = heroDefinition.DefaultPointsValue
                };
                
                m_HeroDatas.Add(heroDefinition.Id, heroData);
            }
            
            UnlockHero(m_PlayerDefinition.DefaultPlayerHero);
            SetHeroPoints(m_PlayerDefinition.DefaultPlayerHero, m_PlayerDefinition.DefaultPlayerHeroPoints);
        }

        public List<string> GetUnlockedHeroes()
        {
            var unlockedHeroes = new List<string>();
            foreach (var heroData in m_HeroDatas)
            {
                if (!heroData.Value.Locked)
                {
                    unlockedHeroes.Add(heroData.Value.Id);
                }
            }

            return unlockedHeroes;
        }

        public int GetHeroPoints(string heroId)
        {
            if (!GetHeroData(heroId, out var heroData))
            {
                return m_DefaultHeroPoints;
            }
            
            return heroData?.Points ?? m_DefaultHeroPoints;
        }

        public void ChangeHeroPoints(string heroId, int delta)
        {
            if (!GetHeroData(heroId, out var heroData))
            {
                return;
            }

            if (delta <= 0 || !GetIsHeroLocked(heroId))
            {
                heroData.Points += delta;
            }
            
            PointsChangeEventProducer.NotifyAll(new HeroPointsChangeArgs(heroId, delta > 0, heroData.Points));
        }
        
        public void SetHeroPoints(string heroId, int value)
        {
            if (!GetHeroData(heroId, out var heroData))
            {
                return;
            }

            var oldValue = heroData.Points;
            heroData.Points = value;
            PointsChangeEventProducer.NotifyAll(new HeroPointsChangeArgs(heroId, value > oldValue, heroData.Points));
        }

        public void UnlockHero(string heroId)
        {
            if (!GetHeroData(heroId, out var heroData))
            {
                return;
            }

            heroData.Locked = false;
            LockChangeEventProducer.NotifyAll(new HeroLockChangeArgs(heroId, heroData.Locked));
        }

        private bool GetHeroData(string heroId, out HeroData heroData)
        {
            if (!m_HeroDatas.TryGetValue(heroId, out heroData))
            {
                Debug.LogError($"{nameof(HeroesDataController)} >>> Hero data not found. Id: {heroId}");
                return false;
            }

            return true;
        }

        private bool GetIsHeroLocked(string heroId)
        {
            if (!GetHeroData(heroId, out var heroData))
            {
                return true;
            }

            return heroData.Locked;
        }
    }
}