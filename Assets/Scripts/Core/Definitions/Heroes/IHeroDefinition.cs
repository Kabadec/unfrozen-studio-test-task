namespace Core.Definitions.Heroes
{
    public interface IHeroDefinition
    {
        string Id { get; }
        int DefaultPointsValue { get; }
        IHeroViewDefinition View { get; }
    }
}