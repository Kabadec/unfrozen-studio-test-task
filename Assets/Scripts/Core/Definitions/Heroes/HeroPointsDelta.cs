﻿using System;
using UnityEngine;

namespace Core.Definitions.Heroes
{
    [Serializable]
    public class HeroPointsDelta
    {
        [SerializeField, HeroId] private string m_HeroId;
        [SerializeField] private int m_Delta;

        public string HeroId => m_HeroId;
        public int Delta => m_Delta;
    }
}