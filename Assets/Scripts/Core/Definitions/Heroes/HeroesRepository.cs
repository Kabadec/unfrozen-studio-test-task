﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Core.Definitions.Heroes
{
    [CreateAssetMenu(menuName = "Defs/Repositories/HeroesRepository", fileName = "HeroesRepository")]
    public class HeroesRepository : ScriptableObject
    {
        [SerializeField] private List<HeroDefinition> m_Heroes;
#if UNITY_EDITOR
        [SerializeField] private Object m_Folder;
#endif
        public IReadOnlyList<IHeroDefinition> Heroes => m_Heroes;

        public IHeroDefinition Get(string id)
        {
            if (string.IsNullOrEmpty(id))
                return default;

            return m_Heroes.FirstOrDefault(heroDefinition => heroDefinition.Id == id);
        }
        
#if UNITY_EDITOR
        [ContextMenu("Update Heroes")]
        private void UpdateHeroes()
        {
            m_Heroes = Utilities.AssetDatabaseExtensions.LoadAssetsFromFolder<HeroDefinition>(m_Folder);
        }
#endif
    }
}