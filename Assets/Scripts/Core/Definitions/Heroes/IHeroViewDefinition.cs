using UnityEngine;

namespace Core.Definitions.Heroes
{
    public interface IHeroViewDefinition
    {
        public string Name { get; }
        public Sprite Icon { get; }
    }
}