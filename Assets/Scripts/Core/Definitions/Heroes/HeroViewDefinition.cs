using System;
using UnityEngine;

namespace Core.Definitions.Heroes
{
    [Serializable]
    public class HeroViewDefinition : IHeroViewDefinition
    {
        [SerializeField] private string m_Name;
        [SerializeField] private Sprite m_Icon;
        
        public string Name => m_Name;
        public Sprite Icon => m_Icon;
    }
}