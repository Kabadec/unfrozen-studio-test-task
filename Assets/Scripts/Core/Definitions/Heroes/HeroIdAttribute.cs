﻿using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Core.Definitions.Heroes
{
    public class HeroIdAttribute : PropertyAttribute
    {
        
    }
    
#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(HeroIdAttribute))]
    public class HeroIdAttributeDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var defs = DefsFacade.I.Heroes.Heroes;
            var ids = defs.Select(heroDefinition => heroDefinition.Id).ToList();
            var index = Mathf.Max(ids.IndexOf(property.stringValue), 0);
            index = EditorGUI.Popup(position, property.displayName, index, ids.ToArray());
            property.stringValue = ids[index];
        }
    }
#endif
}