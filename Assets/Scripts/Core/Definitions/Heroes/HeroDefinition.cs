﻿using UnityEngine;

namespace Core.Definitions.Heroes
{
    [CreateAssetMenu(menuName = "Defs/Hero", fileName = "Hero")]
    public class HeroDefinition : ScriptableObject, IHeroDefinition
    {
        [SerializeField] private string m_Id;
        [SerializeField] private int m_DefaultPointsValue;
        [SerializeField] private HeroViewDefinition m_View;

        public string Id => m_Id;
        public int DefaultPointsValue => m_DefaultPointsValue;
        public IHeroViewDefinition View => m_View;
        
#if UNITY_EDITOR
        [ContextMenu("Apply Name")]
        private void ApplyName()
        {
            var assetPath = UnityEditor.AssetDatabase.GetAssetPath(this);
            UnityEditor.AssetDatabase.RenameAsset(assetPath, m_Id.ToString());
            UnityEditor.AssetDatabase.Refresh();
        }
#endif
    }
}