﻿using Core.Definitions.Heroes;
using UnityEngine;

namespace Core.Definitions.Player
{
    [CreateAssetMenu(menuName = "Defs/PlayerDefinition", fileName = "PlayerDefinition")]
    public class PlayerDefinition : ScriptableObject
    {
        [SerializeField, HeroId] private string m_DefaultPlayerHero;
        [SerializeField] private int m_DefaultPlayerHeroPoints;

        public string DefaultPlayerHero => m_DefaultPlayerHero;
        public int DefaultPlayerHeroPoints => m_DefaultPlayerHeroPoints;
    }
}