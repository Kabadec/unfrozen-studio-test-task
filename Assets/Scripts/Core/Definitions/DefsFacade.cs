﻿using Core.Definitions.Heroes;
using Core.Definitions.Missions;
using Core.Definitions.Player;
using UnityEngine;

namespace Core.Definitions
{
    [CreateAssetMenu(menuName = "Defs/DefsFacade", fileName = "DefsFacade")]
    public class DefsFacade : ScriptableObject
    {
        [SerializeField] private HeroesRepository m_HeroesRepository;
        [SerializeField] private MissionsRepository m_MissionsRepository;
        [SerializeField] private PlayerDefinition m_PlayerDefinition;

        private static DefsFacade m_Instance;

        public HeroesRepository Heroes => m_HeroesRepository;
        public MissionsRepository Missions => m_MissionsRepository;
        public PlayerDefinition PlayerDefinition => m_PlayerDefinition;
        public static DefsFacade I => m_Instance == null ? LoadDefs() : m_Instance;

        private static DefsFacade LoadDefs()
        {
            return m_Instance = Resources.Load<DefsFacade>("DefsFacade");
        }
    }
}