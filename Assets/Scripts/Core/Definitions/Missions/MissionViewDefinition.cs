using System;
using UnityEngine;

namespace Core.Definitions.Missions
{
    [Serializable]
    public class MissionViewDefinition : IMissionViewDefinition
    {
        [SerializeField] private string m_Name;
        
        public string Name => m_Name;
    }
}