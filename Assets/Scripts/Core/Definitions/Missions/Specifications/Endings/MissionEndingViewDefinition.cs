using System;
using UnityEngine;

namespace Core.Definitions.Missions.Specifications.Endings
{
    [Serializable]
    public class MissionEndingViewDefinition : IMissionEndingViewDefinition
    {
        [SerializeField, TextArea] private string m_Description;
        
        public string Description => m_Description;
    }
}