using System.Collections;
using System.Collections.Generic;
using Core.Definitions.Heroes;

namespace Core.Definitions.Missions.Specifications.Endings
{
    public interface IMissionEndingDefinition
    {
        IReadOnlyList<string> PlayerSideHeroes { get; }
        IReadOnlyList<string> EnemySideHeroes { get; }
        IReadOnlyList<HeroPointsDelta> HeroPointsDeltas { get; }
        IMissionEndingViewDefinition View { get; }
        IReadOnlyList<string> UnlockedHeroes { get; }
        int SelectedHeroPointsDelta { get; }
    }
}