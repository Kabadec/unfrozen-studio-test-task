namespace Core.Definitions.Missions.Specifications.Endings
{
    public interface IMissionEndingViewDefinition
    {
        string Description { get; }
    }
}