﻿using System;
using System.Collections.Generic;
using Core.Definitions.Heroes;
using UnityEngine;

namespace Core.Definitions.Missions.Specifications.Endings
{
    [Serializable]
    public class MissionEndingDefinition : IMissionEndingDefinition
    {
        [SerializeField, HeroId] private List<string> m_PlayerSideHeroes;
        [SerializeField, HeroId] private List<string> m_EnemySideHeroes;
        [SerializeField] private int m_SelectedHeroPointsDelta;
        [SerializeField] private List<HeroPointsDelta> m_HeroPointsDeltas;
        [SerializeField, HeroId] private List<string> m_UnlockedHeroes;
        [SerializeField] private MissionEndingViewDefinition m_View;

        public IReadOnlyList<string> PlayerSideHeroes => m_PlayerSideHeroes;
        public IReadOnlyList<string> EnemySideHeroes => m_EnemySideHeroes;
        public int SelectedHeroPointsDelta => m_SelectedHeroPointsDelta;
        public IReadOnlyList<HeroPointsDelta> HeroPointsDeltas => m_HeroPointsDeltas;
        public IMissionEndingViewDefinition View => m_View;
        public IReadOnlyList<string> UnlockedHeroes => m_UnlockedHeroes;
    }
}