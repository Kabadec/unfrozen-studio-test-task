using Core.Definitions.Missions.AccessConditions;
using Core.Definitions.Missions.Specifications.Endings;

namespace Core.Definitions.Missions.Specifications
{
    public interface IMissionSpecificationDefinition
    {
        string Id { get; }
        IAccessConditionDefinition AccessCondition { get; }
        IMissionSpecificationViewDefinition View { get; }
        IMissionEndingDefinition Ending { get; }
    }
}