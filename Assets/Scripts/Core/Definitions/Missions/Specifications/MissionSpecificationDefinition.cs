﻿using System;
using Core.Definitions.Missions.AccessConditions;
using Core.Definitions.Missions.Specifications.Endings;
using UnityEngine;

namespace Core.Definitions.Missions.Specifications
{
    [Serializable]
    public class MissionSpecificationDefinition : IMissionSpecificationDefinition
    {
        [SerializeField] private string m_Id;
        [SerializeField] private AccessConditionDefinition m_AccessCondition;
        [SerializeField] private MissionSpecificationViewDefinition m_View;
        [SerializeField] private MissionEndingDefinition m_Ending;

        public string Id => m_Id;
        public IAccessConditionDefinition AccessCondition => m_AccessCondition;
        public IMissionSpecificationViewDefinition View => m_View;
        public IMissionEndingDefinition Ending => m_Ending;
    }
}