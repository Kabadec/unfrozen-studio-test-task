namespace Core.Definitions.Missions.Specifications
{
    public interface IMissionSpecificationViewDefinition
    {
        string Description { get; }
        string ButtonText { get; }
    }
}