﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Core.Definitions.Missions.Specifications
{
    [Serializable]
    public class MissionSpecificationId
    {
        [SerializeField] private string m_MissionId;
        [SerializeField] private string m_SpecificationId;

        public string MissionId => m_MissionId;
        public string SpecificationId => m_SpecificationId;
    }
    
#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(MissionSpecificationId))]
    public class MissionSpecificationIdDrawer : PropertyDrawer
    {
        private SerializedProperty m_MissionIdProperty;
        private SerializedProperty m_SpecificationIdProperty;

        private const string m_AnyMissionDisplayText = "Any Mission";
        private const string m_AnySpecificationDisplayText = "Any Specification";

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            m_MissionIdProperty = property.FindPropertyRelative("m_MissionId");
            m_SpecificationIdProperty = property.FindPropertyRelative("m_SpecificationId");
            EditorGUI.indentLevel = 3;
            position.width /= 2;

            var missions = DefsFacade.I.Missions.Missions;
            var selectedMission = DrawMissionPopup(position, missions);
            position.x += position.width;
            
            var selectedMissionDefinition = missions.First(x => x.Id == selectedMission);
            DrawSpecificationsPopup(position, selectedMissionDefinition.Specifications);            
        }

        private string DrawMissionPopup(Rect rect, IReadOnlyList<IMissionDefinition> missions)
        {
            var missionIds = new List<string>();
            missionIds.Add(string.Empty);
            missionIds = missions.Select(mission => mission.Id).ToList();
            
            var displayedMissionIds = new List<string>();
            displayedMissionIds.Add(m_AnyMissionDisplayText);
            displayedMissionIds = missions.Select(mission => mission.Id).ToList();
            
            var missionIndex = Mathf.Max(missionIds.IndexOf(m_MissionIdProperty.stringValue), 0);
            var newMissionIndex =  EditorGUI.Popup(rect, missionIndex, displayedMissionIds.ToArray());
            m_MissionIdProperty.stringValue = missionIds[newMissionIndex];
            if (missionIndex != newMissionIndex)
            {
                m_SpecificationIdProperty.stringValue = string.Empty;
            }
            
            return missionIds[newMissionIndex];
        }

        private void DrawSpecificationsPopup(Rect rect, IReadOnlyList<IMissionSpecificationDefinition> specifications)
        {
            if (m_MissionIdProperty.stringValue == string.Empty)
            {
                m_SpecificationIdProperty.stringValue = string.Empty;
                EditorGUI.Popup(rect, 0, new List<string>().ToArray());
                return;
            }

            var specificationIds = new List<string>();
            specificationIds.Add(string.Empty);
            specificationIds.AddRange(specifications.Select(specification => specification.Id).ToList());
            
            var displayedSpecificationIds = new List<string>();
            displayedSpecificationIds.Add(m_AnySpecificationDisplayText);
            displayedSpecificationIds.AddRange(specifications.Select(specification => specification.Id).ToList());
            
            var endingIndex = Mathf.Max(specificationIds.IndexOf(m_SpecificationIdProperty.stringValue), 0);
            var newEndingIndex = EditorGUI.Popup(rect, endingIndex, displayedSpecificationIds.ToArray());
            m_SpecificationIdProperty.stringValue = specificationIds[newEndingIndex];
        }
    }
#endif
}