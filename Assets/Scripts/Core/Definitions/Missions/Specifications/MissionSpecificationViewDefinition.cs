using System;
using UnityEngine;

namespace Core.Definitions.Missions.Specifications
{
    [Serializable]
    public class MissionSpecificationViewDefinition : IMissionSpecificationViewDefinition
    {
        [SerializeField, TextArea] private string m_Description;
        [SerializeField] private string m_ButtonText;

        public string Description => m_Description;
        public string ButtonText => m_ButtonText;
    }
}