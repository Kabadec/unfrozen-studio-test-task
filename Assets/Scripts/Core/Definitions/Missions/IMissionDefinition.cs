using System.Collections.Generic;
using Core.Definitions.Missions.AccessConditions;
using Core.Definitions.Missions.Specifications;

namespace Core.Definitions.Missions
{
    public interface IMissionDefinition
    {
        string Id { get; }
        IAccessConditionDefinition AccessCondition { get; }
        IReadOnlyList<IMissionSpecificationDefinition> Specifications { get; }
        IMissionViewDefinition View { get; }
    }
}