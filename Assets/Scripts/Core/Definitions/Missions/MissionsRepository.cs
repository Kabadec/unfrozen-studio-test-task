﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Core.Definitions.Missions
{
    [CreateAssetMenu(menuName = "Defs/Repositories/MissionsRepository", fileName = "MissionsRepository")]
    public class MissionsRepository : ScriptableObject
    {
        [SerializeField] private List<MissionDefinition> m_Missions;
#if UNITY_EDITOR
        [SerializeField] private Object m_Folder;
#endif
        public IReadOnlyList<IMissionDefinition> Missions => m_Missions;

        public IMissionDefinition Get(string id)
        {
            if (string.IsNullOrEmpty(id))
                return default;

            return m_Missions.FirstOrDefault(missionDefinition => missionDefinition.Id == id);
        }
        
#if UNITY_EDITOR
        [ContextMenu("Update Missions")]
        public void UpdateHeroes()
        {
            m_Missions = Utilities.AssetDatabaseExtensions.LoadAssetsFromFolder<MissionDefinition>(m_Folder);
        }
#endif
    }
}