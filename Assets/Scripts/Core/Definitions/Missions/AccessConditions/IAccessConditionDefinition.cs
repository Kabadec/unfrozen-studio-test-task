using System.Collections.Generic;
using Core.Definitions.Missions.Specifications;

namespace Core.Definitions.Missions.AccessConditions
{
    public interface IAccessConditionDefinition
    {
        /// <summary>
        /// Доступ разрешается, если хотябы одно условие из списка выполняется
        /// </summary>
        IReadOnlyList<MissionSpecificationId> AccessCondition { get; }
    }
}