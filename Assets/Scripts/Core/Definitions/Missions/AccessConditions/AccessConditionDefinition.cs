using System;
using System.Collections.Generic;
using Core.Definitions.Missions.Specifications;
using UnityEngine;

namespace Core.Definitions.Missions.AccessConditions
{
    [Serializable]
    public class AccessConditionDefinition : IAccessConditionDefinition
    {
        [SerializeField] private List<MissionSpecificationId> m_AccessCondition;

        public IReadOnlyList<MissionSpecificationId> AccessCondition => m_AccessCondition;
    }
}