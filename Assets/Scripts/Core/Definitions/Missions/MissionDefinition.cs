using System.Collections.Generic;
using Core.Definitions.Missions.AccessConditions;
using Core.Definitions.Missions.Specifications;
using UnityEngine;

namespace Core.Definitions.Missions
{
    [CreateAssetMenu(menuName = "Defs/Mission", fileName = "Mission")]
    public class MissionDefinition : ScriptableObject, IMissionDefinition
    {
        [SerializeField] private string m_Id;
        [SerializeField] private AccessConditionDefinition m_AccessCondition;
        [SerializeField] private List<MissionSpecificationDefinition> m_Specifications;
        [SerializeField] private MissionViewDefinition m_View;

        public string Id => m_Id;
        public IAccessConditionDefinition AccessCondition => m_AccessCondition;
        public IReadOnlyList<IMissionSpecificationDefinition> Specifications => m_Specifications;
        public IMissionViewDefinition View => m_View;
        
#if UNITY_EDITOR
        [ContextMenu("Apply Name")]
        private void ApplyName()
        {
            var assetPath = UnityEditor.AssetDatabase.GetAssetPath(this);
            UnityEditor.AssetDatabase.RenameAsset(assetPath, m_Id.ToString());
            UnityEditor.AssetDatabase.Refresh();
        }
#endif
    }
}