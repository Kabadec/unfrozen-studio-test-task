namespace Core.Definitions.Missions
{
    public interface IMissionViewDefinition
    {
        string Name { get; }
    }
}