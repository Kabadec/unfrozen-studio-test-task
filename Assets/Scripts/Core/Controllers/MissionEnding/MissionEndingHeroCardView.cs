﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Controllers.MissionEnding
{
    public class MissionEndingHeroCardView : MonoBehaviour
    {
        [SerializeField] private TMP_Text m_NameText;
        [SerializeField] private Image m_Icon;
        
        public void SetEnable(bool isEnable)
        {
            gameObject.SetActive(isEnable);
        }

        public void SetNameText(string nameText)
        {
            m_NameText.text = nameText;
        }

        public void SetIcon(Sprite icon)
        {
            m_Icon.sprite = icon;
        }
    }
}