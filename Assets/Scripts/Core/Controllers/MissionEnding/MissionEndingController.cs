using System.Linq;
using Core.Definitions.Heroes;
using Core.Definitions.Missions;

namespace Core.Controllers.MissionEnding
{
    public class MissionEndingController
    {
        private readonly MissionEndingWindowController m_MissionEndingWindowController;
        private readonly MissionsRepository m_MissionsRepository;
        private readonly HeroesRepository m_HeroesRepository;

        public MissionEndingController(MissionEndingWindowController missionEndingWindowController,
            MissionsRepository missionsRepository,
            HeroesRepository heroesRepository)
        {
            m_MissionEndingWindowController = missionEndingWindowController;
            m_MissionsRepository = missionsRepository;
            m_HeroesRepository = heroesRepository;
        }
        public void Setup(string missionId, string specificationId)
        {
            var missionDefinition = m_MissionsRepository.Get(missionId);
            var specificationDefinition = missionDefinition.Specifications.FirstOrDefault(x => x.Id == specificationId);

            var missionEndingWindowConfig = new MissionEndingWindowConfig()
            {
                Name = missionDefinition.View.Name,
                Description = specificationDefinition.Ending.View.Description,
                PlayerSideHeroes = specificationDefinition.Ending.PlayerSideHeroes.Select(x => m_HeroesRepository.Get(x).View).ToList(),
                EnemySideHeroes = specificationDefinition.Ending.EnemySideHeroes.Select(x => m_HeroesRepository.Get(x).View).ToList()
            };
            
            m_MissionEndingWindowController.Configure(missionEndingWindowConfig);
        }

        public void Open()
        {
            m_MissionEndingWindowController.Open();
        }
    }
}