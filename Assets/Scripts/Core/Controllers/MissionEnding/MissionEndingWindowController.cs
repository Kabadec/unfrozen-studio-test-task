namespace Core.Controllers.MissionEnding
{
    public class MissionEndingWindowController : IInitializable
    {
        private readonly MissionEndingWindow m_MissionEndingWindow;
        private readonly MissionEndingHeroCardView m_HeroCardPrefab;

        private MissionEndingCardViewsHandler m_PlayerSideHeroCardViewsHandler;
        private MissionEndingCardViewsHandler m_EnemySideHeroCardViewsHandler;

        public MissionEndingWindowController(MissionEndingWindow missionEndingWindow,
            MissionEndingHeroCardView heroCardPrefab)
        {
            m_HeroCardPrefab = heroCardPrefab;
            m_MissionEndingWindow = missionEndingWindow;
        }

        public void Initialize()
        {
            m_PlayerSideHeroCardViewsHandler = new MissionEndingCardViewsHandler(m_HeroCardPrefab, m_MissionEndingWindow.PlayerHeroCardViewsParent);
            m_EnemySideHeroCardViewsHandler = new MissionEndingCardViewsHandler(m_HeroCardPrefab, m_MissionEndingWindow.EnemyHeroCardViewsParent);
            m_MissionEndingWindow.Initialize();
            m_MissionEndingWindow.SetCloseCallback(Close);
            m_MissionEndingWindow.Close();
        }

        public void Configure(IMissionEndingWindowConfig config)
        {
            m_MissionEndingWindow.SetNameText(config.Name);
            m_MissionEndingWindow.SetDescriptionText(config.Description);

            m_PlayerSideHeroCardViewsHandler.Configure(config.PlayerSideHeroes);
            m_EnemySideHeroCardViewsHandler.Configure(config.EnemySideHeroes);
        }

        public void Open()
        {
            m_MissionEndingWindow.Open();
        }

        public void Close()
        {
            m_MissionEndingWindow.Close();
        }
    }
}