using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Controllers.MissionEnding
{
    public class MissionEndingWindow : MonoBehaviour
    {
        [SerializeField] private GameObject m_Root;
        [SerializeField] private TMP_Text m_NameText;
        [SerializeField] private TMP_Text m_DescriptionText;
        [SerializeField] private Button m_CloseButton;
        [SerializeField] private Button m_MissionCompleteButton;
        [SerializeField] private RectTransform m_PlayerHeroCardViewsParent;
        [SerializeField] private RectTransform m_EnemyHeroCardViewsParent;
        
        private Action m_CloseCallback;
        public RectTransform PlayerHeroCardViewsParent => m_PlayerHeroCardViewsParent;
        public RectTransform EnemyHeroCardViewsParent => m_EnemyHeroCardViewsParent;

        public void Initialize()
        {
            m_CloseButton.onClick.AddListener(OnCloseButtonClick);
            m_MissionCompleteButton.onClick.AddListener(OnCloseButtonClick);
        }

        public void SetCloseCallback(Action closeCallback)
        {
            m_CloseCallback = closeCallback;
        }

        public void SetNameText(string nameText)
        {
            m_NameText.text = nameText;
        }

        public void SetDescriptionText(string description)
        {
            m_DescriptionText.text = description;
        }

        public void Open()
        {
            m_Root.SetActive(true);
        }

        public void Close()
        {
            m_Root.SetActive(false);
        }
        
        private void OnCloseButtonClick()
        {
            m_CloseCallback?.Invoke();
        }
    }
}