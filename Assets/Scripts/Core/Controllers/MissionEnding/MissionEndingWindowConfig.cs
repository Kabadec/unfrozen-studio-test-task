﻿using System.Collections.Generic;
using Core.Definitions.Heroes;

namespace Core.Controllers.MissionEnding
{
    public class MissionEndingWindowConfig : IMissionEndingWindowConfig
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public IReadOnlyList<IHeroViewDefinition> PlayerSideHeroes { get; set; }
        public IReadOnlyList<IHeroViewDefinition> EnemySideHeroes { get; set; }
    }
}