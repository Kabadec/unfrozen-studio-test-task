﻿using System.Collections.Generic;
using Core.Definitions.Heroes;

namespace Core.Controllers.MissionEnding
{
    public interface IMissionEndingWindowConfig
    {
        string Name { get; }
        string Description { get; }
        IReadOnlyList<IHeroViewDefinition> PlayerSideHeroes { get; }
        IReadOnlyList<IHeroViewDefinition> EnemySideHeroes { get; }
    }
}