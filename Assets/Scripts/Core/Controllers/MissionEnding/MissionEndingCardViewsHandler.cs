﻿using System.Collections.Generic;
using Core.Definitions.Heroes;
using UnityEngine;

namespace Core.Controllers.MissionEnding
{
    public class MissionEndingCardViewsHandler
    {
        private readonly List<MissionEndingHeroCardView> m_CardViews;
        private readonly MissionEndingHeroCardView m_HeroCardPrefab;
        private readonly RectTransform m_Parent;

        public MissionEndingCardViewsHandler(MissionEndingHeroCardView heroCardPrefab, RectTransform parent)
        {
            m_CardViews = new List<MissionEndingHeroCardView>();
            m_Parent = parent;
            m_HeroCardPrefab = heroCardPrefab;
        }

        public void Configure(IReadOnlyList<IHeroViewDefinition> heroViews)
        {
            DisableAll();
            var countCreateHeroCards = heroViews.Count - m_CardViews.Count;
            for (var i = 0; i < countCreateHeroCards; i++)
            {
                var card = CreateHeroCardView();
                card.SetEnable(false);
            }
            
            for (var i = 0; i < heroViews.Count; i++)
            {
                m_CardViews[i].SetNameText(heroViews[i].Name);
                m_CardViews[i].SetIcon(heroViews[i].Icon);
                m_CardViews[i].SetEnable(true);
            }
        }

        private MissionEndingHeroCardView CreateHeroCardView()
        {
            var card = Object.Instantiate(m_HeroCardPrefab, m_Parent);
            m_CardViews.Add(card);
            return card;
        }

        private void DisableAll()
        {
            foreach (var card in m_CardViews)
            {
                card.SetEnable(false);
            }
        }
    }
}