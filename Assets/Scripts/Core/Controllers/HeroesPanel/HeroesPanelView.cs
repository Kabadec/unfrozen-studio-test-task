﻿using DG.Tweening;
using UnityEngine;

namespace Core.Controllers.HeroesPanel
{
    public class HeroesPanelView : MonoBehaviour
    {
        [SerializeField] private RectTransform m_Root;
        [SerializeField] private Transform m_CardsParent;

        private Tween m_AttractAttentionTween;
        
        public Transform CardsParent => m_CardsParent;

        public void StartAttractAttentionAnimation()
        {
            m_AttractAttentionTween?.Kill();
            var seq = DOTween.Sequence();
            seq.Append(m_Root.DOScale(1.2f, 0.1f));
            seq.Append(m_Root.DOScale(1f, 0.1f));
            m_AttractAttentionTween = seq;
        }
    }
}