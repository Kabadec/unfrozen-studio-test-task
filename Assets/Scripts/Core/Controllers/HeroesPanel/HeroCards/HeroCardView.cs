﻿using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Controllers.HeroesPanel.HeroCards
{
    public class HeroCardView : MonoBehaviour
    {
        private const float m_SelectScale = 1.2f;
        private const float m_DeselectScale = 1f;
        
        [SerializeField] private RectTransform m_Root;
        [SerializeField] private TMP_Text m_NameText;
        [SerializeField] private Image m_Icon;
        [SerializeField] private TMP_Text m_PointsText;
        [SerializeField] private RectTransform m_PointsRoot;
        [SerializeField] private Button m_Button;
        [Space]
        [SerializeField] private Color m_ZeroColor;
        [SerializeField] private Color m_PlusColor;
        [SerializeField] private Color m_MinusColor;

        private Tween m_PointsTween;
        private Action m_SelectButtonCallback;

        public void Initialize()
        {
            m_Button.onClick.AddListener(OnClick);
        }

        public void Select()
        {
            m_Root.localScale = new Vector3(m_SelectScale, m_SelectScale, m_SelectScale);
        }

        public void Deselect()
        {
            m_Root.localScale = new Vector3(m_DeselectScale, m_DeselectScale, m_DeselectScale);
        }

        public void UpdatePoints(int points)
        {
            m_PointsText.text = points.ToString();
            UpdatePointsColor(points);
            StartPointsChangeAnimation();
        }

        public void SetIcon(Sprite icon)
        {
            m_Icon.sprite = icon;
        }

        public void SetNameText(string nameText)
        {
            m_NameText.text = nameText;
        }

        public void SetSelectButtonCallback(Action selectButtonCallback)
        {
            m_SelectButtonCallback = selectButtonCallback;
        }

        private void StartPointsChangeAnimation()
        {
            m_PointsTween?.Kill();
            var seq = DOTween.Sequence();
            seq.Append(m_PointsRoot.DOScale(1.2f, 0.1f));
            seq.Append(m_PointsRoot.DOScale(1f, 0.1f));
            m_PointsTween = seq;
        }

        private void UpdatePointsColor(int points)
        {
            if (points == 0)
            {
                m_PointsText.color = m_ZeroColor;
            }
            else if (points > 0)
            {
                m_PointsText.color = m_PlusColor;
            }
            else if (points < 0)
            {
                m_PointsText.color = m_MinusColor;
            }
        }

        private void OnClick()
        {
            m_SelectButtonCallback?.Invoke();
        }
    }
}