﻿using System;
using Core.Data.Heroes;
using Core.Data.Heroes.Points;
using Core.Definitions.Heroes;

namespace Core.Controllers.HeroesPanel.HeroCards
{
    public class HeroCard : IHeroPointsChangeEventObserver
    {
        private readonly IHeroDefinition m_HeroDefinition;
        private readonly HeroCardView m_HeroCardView;

        private bool m_IsSelected;

        public HeroCard(IHeroDefinition heroDefinition,
            HeroesDataController heroesDataController,
            HeroCardView heroCardView,
            Action<string> selectCallback)
        {
            m_HeroCardView = heroCardView;
            m_HeroDefinition = heroDefinition;

            var points = heroesDataController.GetHeroPoints(heroDefinition.Id);
            m_HeroCardView.Initialize();
            m_HeroCardView.SetNameText(heroDefinition.View.Name);
            m_HeroCardView.SetIcon(heroDefinition.View.Icon);
            m_HeroCardView.UpdatePoints(points);
            m_HeroCardView.SetSelectButtonCallback(() => selectCallback?.Invoke(m_HeroDefinition.Id));
            
            heroesDataController.PointsChangeEventProducer.Attach(this);
        }

        public void Select()
        {
            if (m_IsSelected)
            {
                return;
            }
            
            m_IsSelected = true;
            m_HeroCardView.Select();
        }

        public void Deselect()
        {
            if (!m_IsSelected)
            {
                return;
            }
            
            m_IsSelected = false;
            m_HeroCardView.Deselect();
        }

        public void Notify(HeroPointsChangeArgs args)
        {
            if (m_HeroDefinition.Id != args.HeroId)
            {
                return;
            }
            
            UpdatePoints(args.Points);
        }

        private void UpdatePoints(int points)
        {
            m_HeroCardView.UpdatePoints(points);
        }
    }
}