﻿using System.Collections.Generic;
using Core.Controllers.HeroesPanel.HeroCards;
using Core.Data.Heroes;
using Core.Data.Heroes.Locks;
using Core.Definitions.Heroes;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Core.Controllers.HeroesPanel
{
    public class HeroesPanelController : IInitializable, IHeroLockChangeEventObserver
    {
        private readonly HeroesPanelView m_HeroesPanelView;
        private readonly HeroesDataController m_HeroesDataController;
        private readonly HeroCardView m_HeroCardViewPrefab;
        private readonly HeroesRepository m_HeroesRepository;
        
        private Dictionary<string, HeroCard> m_HeroCards;
        private string m_SelectedHero;
        private bool m_IsSelectingEnable;

        public bool HasSelected => m_SelectedHero != string.Empty;
        public string SelectedHero => m_SelectedHero;

        public HeroesPanelController(HeroesPanelView view,
            HeroesDataController heroesDataController,
            HeroesRepository heroesRepository,
            HeroCardView heroCardViewPrefab)
        {
            m_HeroesRepository = heroesRepository;
            m_HeroCardViewPrefab = heroCardViewPrefab;
            m_HeroesDataController = heroesDataController;
            m_HeroesPanelView = view;
        }

        public void Initialize()
        {
            m_SelectedHero = string.Empty;
            m_HeroCards = new Dictionary<string, HeroCard>();
            var unlockedHeroes = m_HeroesDataController.GetUnlockedHeroes();
            foreach (var unlockedHero in unlockedHeroes)
            {
                AddUnlockedHero(unlockedHero);
            }

            m_HeroesDataController.LockChangeEventProducer.Attach(this);
        }

        public void Select(string heroId)
        {
            if (!m_IsSelectingEnable)
            {
                return;
            }
            
            Deselect();
            
            if (m_HeroCards.TryGetValue(heroId, out var selectedCard))
            {
                selectedCard.Select();
                m_SelectedHero = heroId;
            }
        }

        public void Deselect()
        {
            if (m_HeroCards.TryGetValue(m_SelectedHero, out var selectedCard))
            {
                selectedCard.Deselect();
                m_SelectedHero = string.Empty;
            }
        }

        public void StartAttractAttentionAnimation()
        {
            m_HeroesPanelView.StartAttractAttentionAnimation();
        }

        public void EnableSelecting()
        {
            m_IsSelectingEnable = true;
        }

        public void DisableSelecting()
        {
            m_IsSelectingEnable = false;
            Deselect();
        }

        public void Notify(HeroLockChangeArgs args)
        {
            if (args.Locked)
            {
                return;
            }
            
            AddUnlockedHero(args.HeroId);
        }

        private HeroCard AddUnlockedHero(string unlockedHero)
        {
            if (m_HeroCards.TryGetValue(unlockedHero, out var card))
            {
                return card;
            }

            var heroCardView = Object.Instantiate(m_HeroCardViewPrefab, m_HeroesPanelView.CardsParent);
            var heroCard = new HeroCard(m_HeroesRepository.Get(unlockedHero), m_HeroesDataController, heroCardView, Select);
            m_HeroCards.Add(unlockedHero, heroCard);
            Canvas.ForceUpdateCanvases();
            return heroCard;
        }
    }
}