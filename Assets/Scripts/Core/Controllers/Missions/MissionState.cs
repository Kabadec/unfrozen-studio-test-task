﻿namespace Core.Controllers.Missions
{
    public enum MissionState
    {
        Active,
        Locked,
        Selected,
        Completed
    }
}