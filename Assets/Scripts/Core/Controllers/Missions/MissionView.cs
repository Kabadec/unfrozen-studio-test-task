﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Controllers.Missions
{
    public class MissionView : MonoBehaviour
    {
        [SerializeField] private RectTransform m_Root;
        [SerializeField] private Button m_Button;
        [SerializeField] private Image m_MainImage;
        [Space]
        [SerializeField] private Color m_GreenColor;
        [SerializeField] private Color m_DarkGreenColor;
        [SerializeField] private Color m_GrayColor;
        
        private Tween m_Tween;
        private Action m_ClickCallback;

        public void Initialize()
        {
            m_Button.onClick.AddListener(OnClick);
        }

        public void SetInteractable(bool isInteractable)
        {
            m_Button.interactable = isInteractable;
        }

        public void DeselectAnimation()
        {
            m_Tween?.Kill();
            var seq = DOTween.Sequence();
            seq.Append(m_Root.DOScale(1f, 0.1f));
            m_Tween = seq;
        }

        public void SelectAnimation()
        {
            m_Tween?.Kill();
            var seq = DOTween.Sequence();
            seq.Append(m_Root.DOScale(1.2f, 0.1f));
            m_Tween = seq;
        }

        public void SetGreenColor()
        {
            m_MainImage.color = m_GreenColor;
        }

        public void SetGrayColor()
        {
            m_MainImage.color = m_GrayColor;
        }

        public void SetDarkGreenColor()
        {
            m_MainImage.color = m_DarkGreenColor;
        }

        public void SetClickCallback(Action clickCallback)
        {
            m_ClickCallback = clickCallback;
        }

        private void OnClick()
        {
            m_ClickCallback?.Invoke();
        }
    }
}