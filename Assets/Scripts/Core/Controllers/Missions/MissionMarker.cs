﻿using Core.Definitions.Missions;
using UnityEngine;

namespace Core.Controllers.Missions
{
    [SelectionBase]
    public class MissionMarker : MonoBehaviour
    {
        [SerializeField, MissionId] private string m_Id;
        [SerializeField] private MissionView m_View;

        public string Id => m_Id;
        public MissionView View => m_View;
        
#if UNITY_EDITOR
        [ContextMenu("Apply Name")]
        private void ApplyName()
        {
            gameObject.name = m_Id;
        }

        private void OnValidate()
        {
            if (UnityEditor.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage() == null)
            {
                ApplyName();
            }
        }
#endif
    }
}