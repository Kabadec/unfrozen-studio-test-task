﻿using System;
using Core.Controllers.SelectedMission;
using Core.Data.Missions;
using Core.Definitions.Missions;

namespace Core.Controllers.Missions
{
    public class MissionController : IInitializable, IMissionDataChangeEventObserver, ISelectionChangeEventObserver
    {
        private readonly string m_MissionId;
        private readonly MissionView m_MissionView;
        private readonly MissionsDataController m_MissionsDataController;
        private readonly SelectedMissionController m_SelectedMissionController;
        private readonly MissionsRepository m_MissionsRepository;
        
        private IMissionDefinition m_MissionDefinition;
        private MissionState m_State;

        private bool IsSelected => m_SelectedMissionController.IsSelectionOpened &&
                                   m_SelectedMissionController.CurrentMissionId == m_MissionId;

        public MissionController(string missionId,
            MissionView missionView,
            MissionsDataController missionsDataController,
            SelectedMissionController selectedMissionController,
            MissionsRepository missionsRepository)
        {
            m_MissionId = missionId;
            m_MissionView = missionView;
            m_MissionsDataController = missionsDataController;
            m_SelectedMissionController = selectedMissionController;
            m_MissionsRepository = missionsRepository;
        }

        public void Initialize()
        {
            m_MissionDefinition = m_MissionsRepository.Get(m_MissionId);
            m_MissionView.Initialize();
            m_MissionView.SetClickCallback(OnClick);
            m_SelectedMissionController.SelectionChangeEventProducer.Attach(this);
            m_MissionsDataController.DataChangeEventProducer.Attach(this);

            UpdateMissionState(m_MissionsDataController.GetIsCompleted(m_MissionId), IsSelected);
            UpdateView();
        }

        public void Notify(MissionDataChangeEventArgs eventArgs)
        {
            UpdateMissionState(eventArgs.IsCompleted, IsSelected);
            UpdateView();
        }

        public void Notify(SelectionChangeEventArgs args)
        {
            var isCompleted = m_MissionsDataController.GetIsCompleted(m_MissionId);
            var isMissionSelected = args.IsSelectionOpened && args.SelectedMission == m_MissionId;
            
            UpdateMissionState(isCompleted, isMissionSelected);
            UpdateView();
        }

        private void UpdateMissionState(bool isCompleted, bool isSelected)
        {
            if (isCompleted)
            {
                m_State = MissionState.Completed;
                return;
            }
            
            if (m_MissionsDataController.GetIsLocked(m_MissionDefinition.AccessCondition))
            {
                m_State = MissionState.Locked;
                return;
            }

            if (isSelected)
            {
                m_State = MissionState.Selected;
                return;
            }

            m_State = MissionState.Active;
        }

        private void UpdateView()
        {
            switch (m_State)
            {
                case MissionState.Active:
                    SetActiveView();
                    break;
                case MissionState.Locked:
                    SetBlockedView();
                    break;
                case MissionState.Selected:
                    SetSelectedView();
                    break;
                case MissionState.Completed:
                    SetCompletedView();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void OnClick()
        {
            if (m_State != MissionState.Active)
            {
                return;
            }
            
            m_SelectedMissionController.Setup(m_MissionId);
            m_SelectedMissionController.Open();
        }

        private void SetActiveView()
        {
            m_MissionView.SetInteractable(true);
            m_MissionView.SetGreenColor();
            m_MissionView.DeselectAnimation();
        }

        private void SetBlockedView()
        {
            m_MissionView.SetInteractable(false);
            m_MissionView.SetGrayColor();
            m_MissionView.DeselectAnimation();
        }

        private void SetSelectedView()
        {
            m_MissionView.SetInteractable(false);
            m_MissionView.SetGreenColor();
            m_MissionView.SelectAnimation();
        }

        private void SetCompletedView()
        {
            m_MissionView.SetInteractable(false);
            m_MissionView.SetDarkGreenColor();
            m_MissionView.DeselectAnimation();
        }
    }
}