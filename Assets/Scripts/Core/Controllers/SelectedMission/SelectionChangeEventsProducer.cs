﻿using System.Collections.Generic;

namespace Core.Controllers.SelectedMission
{
    public class SelectionChangeEventsProducer
    {
        private readonly List<ISelectionChangeEventObserver> m_Observers = new List<ISelectionChangeEventObserver>();

        public void Attach(ISelectionChangeEventObserver observer)
        {
            m_Observers.Add(observer);
        }
        
        public void Detach(ISelectionChangeEventObserver observer)
        {
            m_Observers.Remove(observer);
        }

        public void NotifyAll(SelectionChangeEventArgs args)
        {
            foreach (var observer in m_Observers)
            {
                observer?.Notify(args);
            }
        }
    }
}