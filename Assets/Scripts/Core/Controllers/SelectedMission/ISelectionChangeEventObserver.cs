﻿namespace Core.Controllers.SelectedMission
{
    public interface ISelectionChangeEventObserver
    {
        void Notify(SelectionChangeEventArgs args);
    }
}