﻿using System.Collections.Generic;
using System.Linq;
using Core.Controllers.HeroesPanel;
using Core.Controllers.MissionEnding;
using Core.Controllers.SelectedMission.Window;
using Core.Data.Heroes;
using Core.Data.Missions;
using Core.Definitions.Missions;
using Core.Definitions.Missions.Specifications;

namespace Core.Controllers.SelectedMission
{
    public class SelectedMissionController : IInitializable
    {
        private readonly HeroesPanelController m_HeroesPanelController;
        private readonly SelectedMissionWindowsController m_SelectedMissionWindowsController;
        private readonly MissionsRepository m_MissionsRepository;
        private readonly MissionsDataController m_MissionsDataController;
        private readonly HeroesDataController m_HeroesDataController;
        private readonly MissionEndingController m_MissionEndingController;

        private string m_CurrentMissionId;
        private bool m_IsSelectionOpened;
        private SelectedMissionWindowConfigurator m_SelectedMissionWindowConfigurator;

        public string CurrentMissionId => m_CurrentMissionId;
        public bool IsSelectionOpened => m_IsSelectionOpened;

        public SelectionChangeEventsProducer SelectionChangeEventProducer { get; private set; }

        public SelectedMissionController(HeroesPanelController heroesPanelController,
            SelectedMissionWindowsController selectedMissionWindowsController,
            MissionsRepository missionsRepository,
            MissionsDataController missionsDataController,
            HeroesDataController heroesDataController,
            MissionEndingController missionEndingController)
        {
            m_HeroesPanelController = heroesPanelController;
            m_SelectedMissionWindowsController = selectedMissionWindowsController;
            m_MissionsRepository = missionsRepository;
            m_MissionsDataController = missionsDataController;
            m_HeroesDataController = heroesDataController;
            m_MissionEndingController = missionEndingController;
        }

        public void Initialize()
        {
            m_SelectedMissionWindowConfigurator = new SelectedMissionWindowConfigurator(OnSpecificationSelect);
            SelectionChangeEventProducer = new SelectionChangeEventsProducer();
            m_SelectedMissionWindowsController.SetCloseCallback(Close);
        }
        
        public void Setup(string missionId)
        {
            m_CurrentMissionId = missionId;
            var missionDefinition = m_MissionsRepository.Get(missionId);
            var accessedSpecifications = new List<IMissionSpecificationDefinition>();
            
            foreach (var specification in missionDefinition.Specifications)
            {
                if (m_MissionsDataController.GetIsLocked(specification.AccessCondition))
                {
                    continue;
                }
                
                accessedSpecifications.Add(specification);
            }

            var windowConfig = m_SelectedMissionWindowConfigurator.GetConfig(missionDefinition, accessedSpecifications);
            m_SelectedMissionWindowsController.Configure(windowConfig);
            SelectionChangeEventProducer.NotifyAll(new SelectionChangeEventArgs(m_IsSelectionOpened, m_CurrentMissionId));
        }

        public void Open()
        {
            if (m_IsSelectionOpened)
            {
                return;
            }
            
            m_HeroesPanelController.EnableSelecting();
            m_SelectedMissionWindowsController.Open();
            m_IsSelectionOpened = true;
            SelectionChangeEventProducer.NotifyAll(new SelectionChangeEventArgs(m_IsSelectionOpened, m_CurrentMissionId));
        }

        public void Close()
        {
            if (!m_IsSelectionOpened)
            {
                return;
            }
            
            m_HeroesPanelController.DisableSelecting();
            m_SelectedMissionWindowsController.Close();
            m_IsSelectionOpened = false;
            SelectionChangeEventProducer.NotifyAll(new SelectionChangeEventArgs(m_IsSelectionOpened, m_CurrentMissionId));
        }

        private void OnSpecificationSelect(string specificationId)
        {
            if (!m_HeroesPanelController.HasSelected)
            {
                m_HeroesPanelController.StartAttractAttentionAnimation();
                return;
            }

            CompleteMission(specificationId);
        }

        private void CompleteMission(string specificationId)
        {
            m_MissionsDataController.CompleteMission(m_CurrentMissionId, specificationId);

            var ending = m_MissionsRepository
                .Get(m_CurrentMissionId)
                .Specifications
                .First(x => x.Id == specificationId)
                .Ending;
            
            m_HeroesDataController.ChangeHeroPoints(m_HeroesPanelController.SelectedHero, ending.SelectedHeroPointsDelta);
            foreach (var endingHeroPointsDelta in ending.HeroPointsDeltas)
            {
                m_HeroesDataController.ChangeHeroPoints(endingHeroPointsDelta.HeroId, endingHeroPointsDelta.Delta);
            }

            foreach (var heroId in ending.UnlockedHeroes)
            {
                m_HeroesDataController.UnlockHero(heroId);
            }

            m_MissionEndingController.Setup(m_CurrentMissionId, specificationId);
            m_MissionEndingController.Open();
            Close();
        }
    }
}