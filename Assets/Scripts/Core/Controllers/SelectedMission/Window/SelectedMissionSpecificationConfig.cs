using System;

namespace Core.Controllers.SelectedMission.Window
{
    public class SelectedMissionSpecificationConfig : ISelectedMissionSpecificationConfig
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string EndingButtonText { get; set; }
        public bool HasAccess { get; set; }
        public Action SelectCallback { get; set; }
    }
}