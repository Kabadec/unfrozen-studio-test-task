using System;

namespace Core.Controllers.SelectedMission.Window
{
    public interface ISelectedMissionSpecificationConfig
    {
        string Name { get; }
        string Description { get; }
        string EndingButtonText { get; }
        bool HasAccess { get; set; }
        Action SelectCallback { get; set; }
    }
}