namespace Core.Controllers.SelectedMission.Window
{
    public class SelectedMissionWindowConfig : ISelectedMissionWindowConfig
    {
        public ISelectedMissionSpecificationConfig LeftSpecification { get; set; }
        public ISelectedMissionSpecificationConfig RightSpecification { get; set; }
    }
}