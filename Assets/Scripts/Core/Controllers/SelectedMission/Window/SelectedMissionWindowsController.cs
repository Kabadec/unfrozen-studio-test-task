﻿using System;

namespace Core.Controllers.SelectedMission.Window
{
    public class SelectedMissionWindowsController : IInitializable
    {
        private readonly SelectedMissionWindow m_LeftSelectedMissionWindow;
        private readonly SelectedMissionWindow m_RightSelectedMissionWindow;

        private bool m_HasLeftWindowAccess;
        private bool m_HasRightWindowAccess;

        private bool m_IsOpened;

        public SelectedMissionWindowsController(
            SelectedMissionWindow leftSelectedMissionWindow,
            SelectedMissionWindow rightSelectedMissionWindow)
        {
            m_RightSelectedMissionWindow = rightSelectedMissionWindow;
            m_LeftSelectedMissionWindow = leftSelectedMissionWindow;
        }

        public void Initialize()
        {
            m_RightSelectedMissionWindow.Initialize();
            m_LeftSelectedMissionWindow.Initialize();
            
            m_RightSelectedMissionWindow.Close();
            m_LeftSelectedMissionWindow.Close();
        }

        public void Configure(ISelectedMissionWindowConfig config)
        {
            m_HasLeftWindowAccess = config.LeftSpecification.HasAccess;
            m_HasRightWindowAccess = config.RightSpecification.HasAccess;
            SetupWindow(m_LeftSelectedMissionWindow, config.LeftSpecification);
            SetupWindow(m_RightSelectedMissionWindow, config.RightSpecification);
        }

        public void SetCloseCallback(Action closeCallback)
        {
            m_LeftSelectedMissionWindow.SetCloseButtonCallback(closeCallback);
            m_RightSelectedMissionWindow.SetCloseButtonCallback(closeCallback);
        }

        public void Open()
        {
            m_IsOpened = true;
            if (m_HasLeftWindowAccess)
            {
                m_LeftSelectedMissionWindow.Open();
            }

            if (m_HasRightWindowAccess)
            {
                m_RightSelectedMissionWindow.Open();
            }
        }

        public void Close()
        {
            m_IsOpened = false;
            m_LeftSelectedMissionWindow.Close();
            m_RightSelectedMissionWindow.Close();
        }

        private void SetupWindow(SelectedMissionWindow window, ISelectedMissionSpecificationConfig config)
        {
            window.SetNameText(config.Name);
            window.SetDescriptionText(config.Description);
            window.SetEndingButtonText(config.EndingButtonText);
            window.SetSelectButtonCallback(config.SelectCallback);
            if (!config.HasAccess)
            {
                window.Close();
            }
            else if (m_IsOpened)
            {
                window.Open();
            }
        }
    }
}