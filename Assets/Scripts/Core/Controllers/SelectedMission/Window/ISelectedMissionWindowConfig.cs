namespace Core.Controllers.SelectedMission.Window
{
    public interface ISelectedMissionWindowConfig
    {
        ISelectedMissionSpecificationConfig LeftSpecification { get; }
        ISelectedMissionSpecificationConfig RightSpecification { get; }
    }
}