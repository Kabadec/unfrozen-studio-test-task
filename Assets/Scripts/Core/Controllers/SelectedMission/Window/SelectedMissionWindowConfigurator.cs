﻿using System;
using System.Collections.Generic;
using Core.Definitions.Missions;
using Core.Definitions.Missions.Specifications;

namespace Core.Controllers.SelectedMission.Window
{
    public class SelectedMissionWindowConfigurator
    {
        private readonly Action<string> m_OnSpecificationSelect;

        public SelectedMissionWindowConfigurator(Action<string> onSpecificationSelect)
        {
            m_OnSpecificationSelect = onSpecificationSelect;
        }

        public ISelectedMissionWindowConfig GetConfig(IMissionDefinition missionDefinition, List<IMissionSpecificationDefinition> accessedSpecifications)
        {
            var leftSpecificationConfig = new SelectedMissionSpecificationConfig();
            var rightSpecificationConfig = new SelectedMissionSpecificationConfig();

            if (accessedSpecifications.Count >= 1)
            {
                var leftSpecificationDefinition = accessedSpecifications[0];
                leftSpecificationConfig = new SelectedMissionSpecificationConfig()
                {
                    Name = missionDefinition.View.Name,
                    Description = leftSpecificationDefinition.View.Description,
                    EndingButtonText = leftSpecificationDefinition.View.ButtonText,
                    HasAccess = true,
                    SelectCallback = () => m_OnSpecificationSelect?.Invoke(leftSpecificationDefinition.Id)
                };
            }
            
            if (accessedSpecifications.Count >= 2)
            {
                var rightSpecificationDefinition = accessedSpecifications[1];
                rightSpecificationConfig = new SelectedMissionSpecificationConfig()
                {
                    Name = missionDefinition.View.Name,
                    Description = rightSpecificationDefinition.View.Description, 
                    EndingButtonText = rightSpecificationDefinition.View.ButtonText,
                    HasAccess = true,
                    SelectCallback = () => m_OnSpecificationSelect?.Invoke(rightSpecificationDefinition.Id)
                };
            }

            return new SelectedMissionWindowConfig()
                {
                    LeftSpecification = leftSpecificationConfig,
                    RightSpecification = rightSpecificationConfig
                };
        }
    }
}