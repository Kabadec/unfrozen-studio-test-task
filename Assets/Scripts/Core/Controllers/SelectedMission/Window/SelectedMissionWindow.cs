﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Controllers.SelectedMission.Window
{
    public class SelectedMissionWindow : MonoBehaviour
    {
        [SerializeField] private GameObject m_Root;
        [SerializeField] private TMP_Text m_NameText;
        [SerializeField] private TMP_Text m_DescriptionText;
        [SerializeField] private TMP_Text m_EndingButtonText;
        [SerializeField] private Button m_EndingButton;
        [SerializeField] private Button m_CloseButton;
        
        private Action m_SelectButtonCallback;
        private Action m_CloseButtonCallback;

        public void Initialize()
        {
            m_EndingButton.onClick.AddListener(OnSelectButtonClick);
            m_CloseButton.onClick.AddListener(OnCloseButtonClick);
        }
        public void Open()
        {
            m_Root.SetActive(true);
        }

        public void Close()
        {
            m_Root.SetActive(false);
        }

        public void SetNameText(string nameText)
        {
            m_NameText.text = nameText;
        }

        public void SetDescriptionText(string description)
        {
            m_DescriptionText.text = description;
        }

        public void SetEndingButtonText(string selectButtonText)
        {
            m_EndingButtonText.text = selectButtonText;
        }

        public void SetSelectButtonCallback(Action selectButtonCallback)
        {
            m_SelectButtonCallback = selectButtonCallback;
        }
        
        public void SetCloseButtonCallback(Action closeButtonCallback)
        {
            m_CloseButtonCallback = closeButtonCallback;
        }
        
        private void OnSelectButtonClick()
        {
            m_SelectButtonCallback?.Invoke();
        }

        private void OnCloseButtonClick()
        {
            m_CloseButtonCallback?.Invoke();
        }
    }
}