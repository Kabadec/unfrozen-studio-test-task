﻿namespace Core.Controllers.SelectedMission
{
    public class SelectionChangeEventArgs
    {
        public bool IsSelectionOpened { get; private set; }
        public string SelectedMission { get; private set; }

        public SelectionChangeEventArgs(bool isSelectionOpened, string selectedMission)
        {
            IsSelectionOpened = isSelectionOpened;
            SelectedMission = selectedMission;
        }
    }
}