﻿using System.Collections.Generic;

namespace Utilities
{
    public static class AssetDatabaseExtensions
    {
#if UNITY_EDITOR
        public static List<TAssetType> LoadAssetsFromFolder<TAssetType>(UnityEngine.Object folder) where TAssetType : UnityEngine.Object
        {
            var folderPath = UnityEditor.AssetDatabase.GetAssetPath(folder);
            var guids = UnityEditor.AssetDatabase.FindAssets("", new[] { folderPath });
            
            var itemList = new List<TAssetType>();
            foreach (var t in guids)
            {
                var path = UnityEditor.AssetDatabase.GUIDToAssetPath(t);
                var asset = UnityEditor.AssetDatabase.LoadAssetAtPath<TAssetType>(path);
                if(asset != null)
                    itemList.Add(asset);
            }

            return itemList;
        }
#endif
    }
}